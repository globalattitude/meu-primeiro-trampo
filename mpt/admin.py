from django.contrib import admin

from .models import (
    MptProfile,
    Assistance,
    MentorStudent,
    Company,
    Vacancy,
    SkillsOption,
    KnowledgeOption
)


admin.site.register(MptProfile)
admin.site.register(Assistance)
admin.site.register(MentorStudent)
admin.site.register(Company)
admin.site.register(Vacancy)
admin.site.register(SkillsOption)
admin.site.register(KnowledgeOption)
