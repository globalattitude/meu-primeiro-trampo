from django.conf.urls import url, include
from django.views.generic import TemplateView
from rest_framework import routers

from . import views


router = routers.SimpleRouter(trailing_slash=False)

app_name = 'mpt'


router.register(r'mpt_profile', views.MptProfileViewSet, base_name='mpt_profile')
router.register(r'assistance', views.AssistanceViewSet, base_name='assistance')
router.register(r'mentor_student', views.MentorStudentViewSet, base_name='mentor_student')
router.register(r'company', views.CompanyViewSet, base_name='company')
router.register(r'vacancy', views.VacancyViewSet, base_name='vacancy')
router.register(r'skills_option', views.SkillsOptionViewSet, base_name='skills_option')
router.register(r'knowledge_option', views.KnowledgeOptionViewSet, base_name='mpt_profile')
router.register(r'vacancy_favorite', views.VacancyFavoriteViewSet, base_name='vacancy_favorite')


urlpatterns = [
    # include new baquara URLs before baquaras
    url(r'^', include('courses_frontend.urls')),

    # include baquara URLs
    url(r'^', include('config.urls')),

    # customized apis
    url(r'^api/', include(router.urls)),

    # Enhance profiles
    url(r'^complete-profile.template.html', TemplateView.as_view(template_name='components/profile/complete-profile.html'), name='complete-profile'),

    # Job openings
    url(r'^opening-candidates.template.html', TemplateView.as_view(template_name='components/openings/opening-candidates.html'), name='opening-candidates'),
    url(r'^opening-card.template.html', TemplateView.as_view(template_name='components/openings/opening-card.html'), name='opening-card'),
    url(r'^opening-detail.template.html', TemplateView.as_view(template_name='components/openings/opening-detail.html'), name='opening-detail'),
    url(r'^opening-edit.template.html', TemplateView.as_view(template_name='components/openings/opening-edit.html'), name='opening-edit'),
    url(r'^openings-list.template.html', TemplateView.as_view(template_name='components/openings/openings-list.html'), name='openings-list'),

    # Mentorship
    url(r'^mentorships-list.template.html', TemplateView.as_view(template_name='components/mentorship/mentorships-list.html'), name='mentorships-list'),
]
