from django.contrib.auth import get_user_model

from rest_framework import serializers

from drf_writable_nested.serializers import WritableNestedModelSerializer

from .models import (
    MptProfile,
    Assistance,
    MentorStudent,
    Company,
    ProfileCourse,
    ProfileLanguage,
    Vacancy,
    SkillsOption,
    KnowledgeOption,
    VacancyFavorite,
)

from baquara.users.serializers import (
    SimpleUserSerializer,
)


User = get_user_model()


class AssistanceSerializer(serializers.ModelSerializer):

    mentor_profile = SimpleUserSerializer(read_only=True, source="mentor")

    class Meta:
        model = Assistance
        fields = '__all__'


class CompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = '__all__'


class SkillsOptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = SkillsOption
        fields = '__all__'


class KnowledgeOptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = KnowledgeOption
        fields = '__all__'


class ProfileCourseSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProfileCourse
        fields = '__all__'


class ProfileLanguageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProfileLanguage
        fields = '__all__'


class MptProfileSerializer(WritableNestedModelSerializer):

    languages = ProfileLanguageSerializer(many=True)
    user_profile = SimpleUserSerializer(read_only=True, source="user")
    profile_courses = ProfileCourseSerializer(many=True)

    class Meta:
        model = MptProfile
        fields = '__all__'


# If the student already filled their MptProfile, return it
# Otherwise, return a fallback with similar shape
class StudentSerializer(serializers.BaseSerializer):

    def to_representation(self, user):
        queryset = MptProfile.objects.filter(user=user)

        if queryset.count() > 0:
            mpt_profile = MptProfileSerializer(queryset.first())
            return mpt_profile.data
        else:
            profile = SimpleUserSerializer(user)
            return { 'user_profile': profile.data }


class MentorStudentSerializer(serializers.ModelSerializer):

    student_profile = StudentSerializer(read_only=True, source='user')

    class Meta:
        model = MentorStudent
        fields = '__all__'


class VacancySerializer(WritableNestedModelSerializer):

    company = CompanySerializer()
    favorites_count = serializers.SerializerMethodField()
    user_match = serializers.SerializerMethodField()
    is_current_user_applied = serializers.SerializerMethodField()

    class Meta:
        model = Vacancy
        fields = '__all__'

    def get_favorites_count(self, obj):
        return VacancyFavorite.objects.filter(vacancy=obj.id).count()

    def get_is_current_user_applied(self, obj):
        request = self.context.get("request")

        return obj.is_current_user_applied(user=request.user)

    def get_user_match(self, obj):
        request = self.context.get("request")

        return obj.get_user_match(user=request.user)


class VacancyFavoriteSerializer(WritableNestedModelSerializer):

    class Meta:
        model = VacancyFavorite
        fields = '__all__'
