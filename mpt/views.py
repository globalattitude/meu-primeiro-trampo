from rest_framework import status, viewsets
from rest_framework.decorators import action, detail_route, parser_classes
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response

from .models import (
    MptProfile,
    Assistance,
    MentorStudent,
    Company,
    Vacancy,
    SkillsOption,
    KnowledgeOption,
    VacancyFavorite,
)

from .serializers import (
    MptProfileSerializer,
    AssistanceSerializer,
    StudentSerializer,
    MentorStudentSerializer,
    CompanySerializer,
    VacancySerializer,
    SkillsOptionSerializer,
    KnowledgeOptionSerializer,
    VacancyFavoriteSerializer,
)


class MptProfileViewSet(viewsets.ModelViewSet):

    queryset = MptProfile.objects.all()
    serializer_class = MptProfileSerializer
    lookup_field = 'user'

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class AssistanceViewSet(viewsets.ModelViewSet):

    queryset = Assistance.objects.all()
    serializer_class = AssistanceSerializer
    model = Assistance
    filter_fields = ['user']

    def get_queryset(self):
        queryset = super().get_queryset()

        if not self.request.user.is_superuser:
            queryset = queryset.filter(
                mentor=self.request.user,
            )

        return queryset


class MentorStudentViewSet(viewsets.ModelViewSet):

    queryset = MentorStudent.objects.all()
    serializer_class = MentorStudentSerializer
    filter_fields = ['mentor', 'user']


class CompanyViewSet(viewsets.ModelViewSet):

    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class VacancyViewSet(viewsets.ModelViewSet):

    filter_fields = ['students']
    queryset = Vacancy.objects.all()
    serializer_class = VacancySerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @action(detail=True, methods=['post'])
    def apply(self, request, pk=None):
        vacancy = self.get_object()
        vacancy.students.add(request.user)
        vacancy.save()
        return Response({'count': vacancy.students.count()})

    @action(detail=True, methods=['get'])
    def candidates(self, request, pk=None):
        vacancy = self.get_object()
        students = vacancy.students.all()
        serializer = StudentSerializer(students, many=True)
        return Response(serializer.data)

    @detail_route(methods=['POST'])
    @parser_classes((FormParser, MultiPartParser))
    def image(self, request, *args, **kwargs):
        if 'image' in request.data:
            vacancy = self.get_object()
            vacancy.company.logo.delete()

            upload = request.data['image']
            vacancy.company.logo.save(upload.name, upload)

            return Response(status=status.HTTP_201_CREATED,
                            headers={'Location': vacancy.company.logo.url})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'])
    def view(self, request, pk=None):
        vacancy = self.get_object()
        vacancy.views += 1
        vacancy.save()
        return Response({'views': vacancy.views})


class SkillsOptionViewSet(viewsets.ModelViewSet):

    queryset = SkillsOption.objects.all()
    serializer_class = SkillsOptionSerializer


class KnowledgeOptionViewSet(viewsets.ModelViewSet):

    queryset = KnowledgeOption.objects.all()
    serializer_class = KnowledgeOptionSerializer


class VacancyFavoriteViewSet(viewsets.ModelViewSet):

    queryset = VacancyFavorite.objects.all()
    serializer_class = VacancyFavoriteSerializer
    filter_fields = ['user', 'vacancy']
    # permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)
