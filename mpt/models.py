from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils import timezone

from cities_light.models import City, Region


class SkillsOption(models.Model):
    name = models.CharField(
        _('Option Name'),
        null=True, max_length=255,
        blank=True
    )

    def __str__(self):
        return self.name or ''


class KnowledgeOption(models.Model):
    name = models.CharField(
        _('Option Name'),
        null=True, max_length=255,
        blank=True
    )

    def __str__(self):
        return self.name or ''


WORKDAY_CHOICES = (
    ('full_time', _('Full time')),
    ('part_time_morning', _('Party time - Morning')),
    ('part_time_afternoon', _('Party time - Afternoon')),
    ('part_time_night', _('Party time - Night')),
    ('night_time', _('Night time')),
)

CONTRACT_TYPE_CHOICES = (
    ('effective', _('Effective')),
    ('internship', _('Internship')),
    ('apprentice', _('Apprentice')),
)

PRESENCE_CHOICES = (
    ('presential', _('Presential')),
    ('hybrid', _('Hybrid')),
    ('remote', _('Remote')),
)

class MptProfile(models.Model):

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    school = models.CharField(
        _("School"),
        max_length=500,
        blank=True,
        null=True,
    )

    profile_courses = models.ManyToManyField(
        'ProfileCourse',
        verbose_name=_('Profile Course'),
        blank=True,
    )

    languages = models.ManyToManyField(
        'ProfileLanguage',
        verbose_name=_('Profile Language'),
        blank=True,
    )

    knowledge = models.ManyToManyField(
        KnowledgeOption,
        verbose_name=_('Knowledge'),
        blank=True,
    )

    skills = models.ManyToManyField(
        SkillsOption,
        verbose_name=_('Skills'),
        blank=True,
    )

    resume = models.TextField(
        _('Resume'),
        blank=True,
        null=True,
    )

    # Professional Objectives
    workday = models.CharField(
        _('Workday Time'),
        choices=WORKDAY_CHOICES,
        max_length=255,
        null=True,
        blank=True
    )

    contract_type = models.CharField(
        _('Contract type'),
        choices=CONTRACT_TYPE_CHOICES,
        max_length=255,
        null=True,
        blank=True
    )


class ProfileCourse(models.Model):

    institution_name = models.CharField(
        _("Institution name"),
        max_length=500,
        blank=True,
        null=True,
    )

    course_name = models.CharField(
        _("Course name"),
        max_length=500,
        blank=True,
        null=True,
    )

    duration = models.CharField(
        _("Course duration"),
        max_length=100,
        blank=True,
        null=True,
    )

    workload = models.CharField(
        _("Course work load"),
        max_length=20,
        blank=True,
        null=True,
    )


class ProfileLanguage(models.Model):

    LANGUAGE_CHOICES = (
        ('en', _('English')),
        ('es', _('Spanish')),
        ('fr', _('French')),
        ('it', _('Italian')),
        ('de', _('German')),
        ('jp', _('Japanese')),
        ('ar', _('Arabic')),
    )

    LEVEL_CHOICES = (
        ('beginner', _('Beginner')),
        ('intermediate', _('Intermediate')),
        ('advanced', _('Advanced')),
        ('native', _('Native')),
    )

    language = models.CharField(
        _('Language'),
        choices=LANGUAGE_CHOICES,
        max_length=255,
        null=True,
        blank=True
    )

    level = models.CharField(
        _('Language level'),
        choices=LEVEL_CHOICES,
        max_length=255,
        null=True,
        blank=True
    )


class Assistance(models.Model):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=_('Student'),
        related_name='assistancies',
    )
    mentor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=_('Mentor'),
        related_name='mentor_assistances',
    )
    content = models.TextField(_("content"))
    date = models.DateTimeField(
        default=timezone.now,
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Assistance')

    def __str__(self):
        return self.user.name + self.date


class MentorStudent(models.Model):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=_('Student'),
        related_name='mentors',
    )
    mentor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=_('Menthor'),
        related_name='mentorship',
    )


class Company(models.Model):

    name = models.CharField(
        _("Name"),
        max_length=100,
    )

    email = models.EmailField(
        _("Email"),
        max_length=254,
    )

    phone_number = models.CharField(
        _('Phone number'),
        max_length=255
    )

    logo = models.ImageField(
        _("Logo"),
        upload_to='company_logo_images',
        blank=True,
    )

    contact_name = models.CharField(
        _('Contact name'),
        max_length=255,
        blank=True,
    )

    contact_email = models.CharField(
        _('Contact email'),
        max_length=255,
    )

    contact_link = models.TextField(
        _('Contact link'),
        blank=True,
    )


class Vacancy(models.Model):

    name = models.CharField(
        _('Name'),
        max_length=100,
    )

    description = models.TextField(
        _('Description'),
    )

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=_('Author'),
        related_name='authored_vacancies',
    )

    students = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Student'),
        blank=True,
    )

    company = models.ForeignKey(
        Company,
        models.CASCADE,
        verbose_name=_('Menthor'),
    )

    city = models.ForeignKey(
        City,
        on_delete=models.SET_NULL,
        verbose_name=_('City'),
        related_name='vacancies',
        null=True,
        blank=True,
    )

    state = models.ForeignKey(
        Region,
        on_delete=models.SET_NULL,
        verbose_name=_('Region'),
        related_name='vacancies',
        null=True,
        blank=True
    )

    knowledge = models.ManyToManyField(
        KnowledgeOption,
        verbose_name=_('Knowledge'),
        blank=True,
    )

    skills = models.ManyToManyField(
        SkillsOption,
        verbose_name=_('Skills'),
        blank=True,
    )

    salary = models.CharField(
        _('Salary'),
        max_length=255,
        blank=True,
    )

    salary_to_be_defined = models.BooleanField(
        _('Salary to be negotiated'),
        default=False,
    )

    # Professional Objectives
    workday = models.CharField(
        _('Workday Time'),
        choices=WORKDAY_CHOICES,
        max_length=255,
    )

    contract_type = models.CharField(
        _('Contract type'),
        choices=CONTRACT_TYPE_CHOICES,
        max_length=255,
    )

    presence = models.CharField(
        _('Presence'),
        choices=PRESENCE_CHOICES,
        max_length=255,
    )

    created_at = models.DateTimeField(auto_now_add=True)

    views = models.IntegerField(
        _('Views'),
        default=0,
    )

    def get_user_match(self, user):
        import random

        return random.randint(3, 5)

    def is_current_user_applied(self, user):
        return user in self.students.all()


class VacancyFavorite(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=_('User')
    )

    timestamp = models.DateTimeField(auto_now=True)

    vacancy = models.ForeignKey(
        Vacancy,
        models.CASCADE,
        related_name='uses_favorite'
    )

    class Meta:
        unique_together = ('user', 'vacancy')
