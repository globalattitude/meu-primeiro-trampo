(function(angular) {
    'use strict';

    var app = angular.module('baquara');

    app.config(['$stateProvider', '$urlServiceProvider', function($stateProvider, $urlServiceProvider) {

      // Unregister routes that will be overridden
      $stateProvider.stateRegistry.deregister('dashboard');
      $stateProvider.stateRegistry.deregister('profile');

      // Define new or overridden routes
      $stateProvider
      .state({
        name: 'profile',
        url: '/profile/{userId}',
        component: 'profile',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load([
            'https://unpkg.com/moment@2.29.1/min/moment-with-locales.min.js',
            'https://unpkg.com/moment-timezone@0.5.34/builds/moment-timezone-with-data-10-year-range.min.js',
            'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.js',
            'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.css',

            '/static/js/app/openings/openings.services.js',
            '/static/js/app/openings/opening-card/opening-card.module.js',
            '/static/js/app/openings/opening-card/opening-card.component.js',
          ], { serie: true });
        }
      })
      .state({
        name: 'completeProfile',
        url: '/complete-profile',
        component: 'completeProfile',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load([
            '/static/js/app/profile/complete-profile/complete-profile.module.js',
            '/static/js/app/profile/complete-profile/complete-profile.component.js',
          ], {
            serie: true,
          });
        },
      })
      .state({
        name: 'openings.**',
        url: '/openings',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load([
            '/static/js/app/openings/openings.module.js',
          ]);
        },
      })
      .state({
        name: 'mentorship.**',
        url: '/mentorship',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load([
            '/static/js/app/mentorship/mentorship.module.js',
          ]);
        },
      })
      .state({
        name: 'dashboard',
        url: '/dashboard',
        component: 'dashboard',
        lazyLoad: function ($transition$) {
          return $transition$.injector().get('$ocLazyLoad').load([
            'https://unpkg.com/moment@2.29.1/min/moment-with-locales.min.js',
            'https://unpkg.com/moment-timezone@0.5.34/builds/moment-timezone-with-data-10-year-range.min.js',
            'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.js',
            'https://unpkg.com/angular-moment-picker@0.10.2/dist/angular-moment-picker.min.css',

            '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.module.js',
            '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.services.js',
            '/static/js/app/i18n/edit-language-switcher/edit-language-switcher.component.js',

            '/static/js/app/accounts/accounts.services.js',
            '/static/js/app/events/events.module.js',
            '/static/js/app/events/events.services.js',
            '/static/js/app/events/events-list/events-list.module.js',
            '/static/js/app/events/events-list/events-list.component.js',
            // '/static/js/app/cards/he.filters.js',
            '/static/js/app/openings/openings.services.js',
            '/static/js/app/openings/opening-card/opening-card.module.js',
            '/static/js/app/openings/opening-card/opening-card.component.js',

            '/static/js/app/openings/openings.services.js',
            '/static/js/app/dashboard/mpt.dashboard.module.js',
            '/static/js/app/dashboard/mpt.dashboard.component.js',
            // '/static/js/app/dashboard/dashboard.component.js',
          ], {
            serie: true,
          },);
        },
      });
    }]);
})(angular);
