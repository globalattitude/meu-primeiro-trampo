(function (angular) {
    'use strict';
    var module = angular.module('profile.services', ['ngResource']);

    module.factory('UserProfile', function($resource){
        return $resource(BASE_API_URL + '/profile/:userId', {}, {
        });
    });

    module.factory('Assistance', function($resource) {
        return $resource(API_URL + '/assistance', {}, {
        });
    });

    module.factory('City', function($resource){
        return $resource(API_URL + '/address/cities/:id', {}, {
        });
    });

    module.factory('CityAutocomplete', ['$http', function($http){
        return function(val) {
            return $http.get(API_URL + '/address/autocomplete/city/', {
                params: {
                    q: val,
                }
            }).then(function (res) {
                return res.data.results;
            });
        };
    }]);

    module.factory('KnowledgeOptions', function($resource) {
        return $resource(API_URL + '/knowledge_option', {}, {
        });
    });

    module.factory('MentorStudent', function($resource) {
        return $resource(API_URL + '/mentor_student', {}, {
        });
    });

    module.factory('MptProfile', function($resource) {
        return $resource(API_URL + '/mpt_profile/:user', { 'user': '@user' }, {
            'save': { method: 'POST', url: API_URL + '/mpt_profile' },
            'update': { method: 'PUT' },
        });
    });

    module.factory('SkillOptions', function($resource) {
        return $resource(API_URL + '/skills_option', {}, {
        });
    });

})(angular);
