(function(angular) {
    'use strict';

    angular
        .module('completeProfile')
        .component('completeProfile', {
            templateUrl: '/complete-profile.template.html',
            controller: CompleteProfileCtrl,
            bindings: {
            },
        });

    CompleteProfileCtrl.$inject = [
        '$scope',
        '$state',
        'City',
        'CityAutocomplete',
        'CurrentUser',
        'KnowledgeOptions',
        'MptProfile',
        'SkillOptions',
        'UserProfile',
        'gettextCatalog',
    ];

    function CompleteProfileCtrl (
        $scope,
        $state,
        City,
        CityAutocomplete,
        CurrentUser,
        KnowledgeOptions,
        MptProfile,
        SkillOptions,
        UserProfile,
        gettextCatalog,
    ) {
        const ctrl = this;

        ctrl.user = CurrentUser;
        ctrl.profile = null;
        ctrl.cityName = '';
        ctrl.errors = {};

        UserProfile.get({ username: ctrl.user.username }, function (profile) {
            ctrl.profile = profile;

            if (profile.city){
                City.get({ id: profile.city }, function(city) {
                    ctrl.cityName = city.display_name;
                });
            }

            MptProfile.get({ user: profile.id }, function (mptProfile) {
                ctrl.mode = 'edit';
                ctrl.mptProfile = mptProfile;
            }, function () {
                ctrl.mode = 'create';
                ctrl.mptProfile = {
                    school: null,
                    resume: '',
                    workday: null,
                    contract_type: null,
                    user: Number(profile.id),
                    profile_courses: [newCourse()],
                    languages: [newLanguage()],
                    knowledge: [],
                    skills: [],
                };
            });
        });

        KnowledgeOptions.query({}, function (knowledgeOptions) {
            ctrl.knowledgeOptions = knowledgeOptions;
        });

        SkillOptions.query({}, function (skillOptions) {
            ctrl.skillOptions = skillOptions;
        });

        ctrl.mptProfile = null;
        ctrl.schoolCityName = '';

        function getOptions (dict) {
            return Object.entries(dict).map((entry) => {
                return { label: gettextCatalog.getString(entry[1]), slug: entry[0] };
            });
        }

        function newCourse () {
            return {
                course_name: '',
                institution_name: '',
                duration: null,
            };
        }

        function newLanguage () {
            return {
                language: null,
                level: null,
            };
        }

        ctrl.options = {
            contract_type: getOptions({
                'effective': 'Effective',
                'internship': 'Internship',
                'apprentice': 'Apprentice',
            }),
            workday: getOptions({
                'full_time': 'Full time',
                'part_time_morning': 'Party time - Morning',
                'part_time_afternoon': 'Party time - Afternoon',
                'part_time_night': 'Party time - Night',
                'night_time': 'Night time',
            }),
        };

        ctrl.$onInit = function () {

        };

        ctrl.addCourse = function () {
            ctrl.mptProfile.profile_courses.push(newCourse());
        };

        ctrl.addLanguage = function () {
            ctrl.mptProfile.languages.push(newLanguage());
        };

        ctrl.getCities = function (val) {
            return new CityAutocomplete(val);
        };

        ctrl.onSelectCity = function (val) {
            ctrl.mptProfile.school_city = val.id;
        };

        ctrl.saveProfile = function () {
            ctrl.mptProfile.languages = ctrl.mptProfile.languages.filter((language) => language.language && language.level);
            ctrl.mptProfile.profile_courses = ctrl.mptProfile.profile_courses.filter((course) => course.course_name && course.institution_name && course.duration);

            const action = (ctrl.mode === 'edit') ? 'update': 'save';

            MptProfile[action](ctrl.mptProfile, function (profile) {
                ctrl.mptProfile = profile;
                ctrl.errors = {};
                $state.go('profile', { userId: ctrl.user.username });
            }, function (errors) {
                console.error(errors);
                ctrl.errors = errors.data;
            })
        };
    }
})(angular);
