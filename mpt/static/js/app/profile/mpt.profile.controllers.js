(function(angular){
    'use strict';
    var app = angular.module('profile.controllers', ['ngSanitize']);

    app.controller('UserProfileController', [
        '$scope',
        '$location',
        '$uibModal',
        'UserProfile',
        'CurrentUser',
        'UserAccess',
        'MyCourses',
        'Assistance',
        'CourseStudent',
        'CityAutocomplete',
        'City',
        'KnowledgeOptions',
        'MentorStudent',
        'MptProfile',
        'SkillOptions',
        'TranslationService',
        'Vacancy',
        'gettextCatalog',

        function ($scope, $location, $uibModal, TimtecUser, CurrentUser, UserAccess, MyCourses, Assistance, CourseStudent, CityAutocomplete, City, KnowledgeOptions, MentorStudent, MptProfile, SkillOptions, TranslationService, Vacancy, gettextCatalog) {

            const locale = TranslationService.currentLanguage;

            const tinymceOptions = {
                plugins: 'link',
                toolbar: 'undo redo | bold italic | link removeformat',
            };

            $scope.language_options = [
                {id:'', name:''},
                {id:'pt-br', name:'PT-BR'},
                {id:'en', name:'EN'},
                {id:'es', name:'ES'}
            ];

            // Log this access
            (new UserAccess({ area: 'profile-page' })).$save().then(
                (success) => {},
                (error) => {
                    console.warn("The area: 'profile-page' was not registered in UserAccess")
                }
            );

            // Set buttons for the tinyMCE editor bio field
            $scope.tinymceOptions = {
                toolbar: 'bold italic | bullist numlist outdent indent | quicklink link | removeformat'
              };

            $scope.current_user = CurrentUser;
            $scope.mpt_profile = null;

            $scope.save_profile = function(username) {
                return confirm(`${gettextCatalog.getString('You confirm the change to the user:')} ${username}?`);
            }

            $scope.get_tag_label = function (id, list) {
                return list.find((item) => item.id == id)?.name ?? id;
            };

            // remove the last slash
            var abs_url = $location.absUrl();
            if (abs_url.slice(-1) === '/') {
                abs_url = abs_url.slice(0, -1);
            }

            var username = abs_url.split('/').pop();

            if (username === 'profile' || username === 'edit') {
                username = CurrentUser.username;
            }

            $scope.cityName = '';

            $scope.getCities = function(val) {
                return new CityAutocomplete(val)
            };

            $scope.on_select_city = function(val) {
                if ($scope.user_profile == null ){
                    $scope.user_profile = {
                    }
                }
                $scope.user_profile.city = val.id;
            };

            TimtecUser.get({username: username}, function(user_profile) {

                if (user_profile.city){
                    City.get({id: user_profile.city}, function(city) {
                        $scope.cityName = city.display_name;
                    });
                }
                else{
                    $scope.cityName ='';
                }
                user_profile.is_current_user = (user_profile.id === parseInt(CurrentUser.id, 10));
                $scope.user_profile = user_profile;

                if (user_profile.birth_date) {
                    $scope.user_age = moment().diff(user_profile.birth_date, 'years');
                }

                // Try to find at least one valid certificate
                $scope.valid_certificates = false;
                for (var i = 0; i < user_profile.certificates.length; i++) {
                    if(user_profile.certificates[i].approved === true){
                        $scope.valid_certificates = true;
                        break;
                    }
                }

                KnowledgeOptions.query({}, function (knowledge_options) {
                    $scope.knowledge_options = knowledge_options;
                });

                SkillOptions.query({}, function (skill_options) {
                    $scope.skill_options = skill_options;
                });

                if ($scope.current_user.is_superuser || user_profile.is_current_user) {
                    Vacancy.query({ students: user_profile.id }, function (openings) {
                        $scope.openings = openings;
                    });
                }

                if ($scope.current_user.is_superuser || $scope.current_user.is_staff) {
                    MentorStudent.query({ mentor: user_profile.id }, function (mentored) {
                        $scope.mentored = mentored;
                    });
                }

                MptProfile.get({ user: user_profile.id }, function (mpt_profile) {
                    $scope.mpt_profile = mpt_profile;
                });

                Assistance.query({ user: user_profile.id }, function(assistances) {
                    $scope.assistances = assistances;
                });

                if (user_profile.is_current_user) {
                    MyCourses.query({}, function (my_courses) {
                        $scope.my_courses = my_courses;

                        CourseStudent.query({}, function (course_students){
                            $scope.course_students = course_students;

                            angular.forEach($scope.my_courses, function(my_course) {
                                $scope.langs = [];
                                angular.forEach(my_course.lang, function(lang) {
                                    let langs_dict = {
                                        'en': 'English',
                                        'es': 'Spanish',
                                        'pt-br': 'Portuguese'
                                    }
                                    $scope.langs.push(langs_dict[lang]);
                                });
                                my_course.lang = $scope.langs;
                                angular.forEach($scope.course_students, function(course_student) {
                                    if (my_course.id === course_student.course.id) {
                                        my_course.course_student = course_student;
                                    }
                                });
                            });
                        });
                    });
                }
            }, function(error) {
                $scope.user_profile = null;
            });

            $scope.createAssistance = function (current_user, student_user) {
                const CreateAssistanceModalInstanceCtrl = ($scope, $uibModalInstance) => {
                    $scope.assistance = {
                        content: '',
                        date: '',
                        mentor: Number(current_user.id),
                        user: student_user.id,
                    };

                    $scope.dateTime = {};
                    $scope.dateTime.str = '';
                    $scope.dateTime.obj = moment().locale(locale);

                    $scope.locale = locale;
                    $scope.tinymceOptions = tinymceOptions;

                    $scope.cancelModal = function () {
                        $uibModalInstance.dismiss();
                    };

                    $scope.saveAssistance = function () {
                        $scope.assistance.date = $scope.dateTime.obj.format();
                        $uibModalInstance.close($scope.assistance);
                    }
                }

                const modalInstance = $uibModal.open({
                    templateUrl: 'create-assistance_modal.html',
                    controller: [
                        '$scope',
                        '$uibModalInstance',
                        CreateAssistanceModalInstanceCtrl,
                    ],
                });

                modalInstance.result.then((newAssistance) => {
                    Assistance.save(newAssistance).$promise.then((newAssistance) => {
                        $scope.assistances.unshift(newAssistance);
                    });
                });
            }
        }
    ]);

    app.directive("file", function () {
      return {
        require: "ngModel",
        restrict: "A",
        link: function ($scope, el, attrs, ngModel) {
          el.bind("change", function (event) {
            const imgtag = document.getElementById("profile-avatar");
            const img = document.createElement("img");
            const selectedFile = event.target.files[0];
            const reader = new FileReader();
            reader.onload = function(event) {
                img.src = event.target.result;
                const canvas = document.createElement("canvas");
                let ctx = canvas.getContext("2d");

                img.onload = () => {
                    ctx.drawImage(img, 0, 0);
                    const MAX_WIDTH = 400;
                    const MAX_HEIGHT = 300;
                    let width = img.width;
                    let height = img.height;

                    if (width > height) {
                        if (width > MAX_WIDTH) {
                            height *= MAX_WIDTH / width;
                            width = MAX_WIDTH;
                        }
                    } else {
                        if (height > MAX_HEIGHT) {
                            width *= MAX_HEIGHT / height;
                            height = MAX_HEIGHT;
                        }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0, width, height);

                    const dataurl = canvas.toDataURL("image/png");
                    imgtag.style.backgroundImage = `url(${dataurl})`;
                }
            };
            reader.readAsDataURL(selectedFile);
            ngModel.$setViewValue(selectedFile);
            $scope.$apply();
          });
        },
      };
    });
})(window.angular);
