(function(angular) {
    'use strict';

    var module = angular.module('openingDetail', [

    ]);

    module.config(['$compileProvider', function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|tel|mailto):/);
    }]);

})(angular);
