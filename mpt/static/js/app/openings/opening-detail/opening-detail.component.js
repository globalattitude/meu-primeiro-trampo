(function(angular) {
    'use strict';

    angular
        .module('openingDetail')
        .component('openingDetail', {
            templateUrl: '/opening-detail.template.html',
            controller: OpeningDetailCtrl,
            bindings: {
                opening: '<',
            },
        });

    OpeningDetailCtrl.$inject = [
        '$scope',
        '$sce',
        '$state',
        '$stateParams',
        'City',
        'CurrentUser',
        'KnowledgeOptions',
        'SkillOptions',
        'Vacancy',
        'VacancyFavorite',
        'gettextCatalog',
    ];

    function OpeningDetailCtrl (
        $scope,
        $sce,
        $state,
        $stateParams,
        City,
        CurrentUser,
        KnowledgeOptions,
        SkillOptions,
        Vacancy,
        VacancyFavorite,
        gettextCatalog,
    ) {
        const ctrl = this;

        ctrl.user = CurrentUser;
        const userId = Number(ctrl.user.id);

        ctrl.openingId = $stateParams.openingId;

        ctrl.favoriteId = null;
        ctrl.mode = ctrl.user.groups.includes('Mentores') ? 'mentor' : 'student';

        KnowledgeOptions.query({}, function (knowledgeOptions) {
            ctrl.knowledgeOptions = knowledgeOptions;
        });

        SkillOptions.query({}, function (skillOptions) {
            ctrl.skillOptions = skillOptions;
        });

        function buildOptions (dict) {
            return Object.entries(dict).map((entry) => {
                return { name: gettextCatalog.getString(entry[1]), id: entry[0] };
            });
        }

        ctrl.options = {
            contract_type: buildOptions({
                'effective': 'Effective',
                'internship': 'Internship',
                'apprentice': 'Apprentice',
            }),
            presence: buildOptions({
                'presential': 'Presential',
                'hybrid': 'Hybrid',
                'remote': 'Remote',
            }),
            workday: buildOptions({
                'full_time': 'Full time',
                'part_time_morning': 'Party time - Morning',
                'part_time_afternoon': 'Party time - Afternoon',
                'part_time_night': 'Party time - Night',
                'night_time': 'Night time',
            }),
        };

        Vacancy.get({ id: ctrl.openingId }, function (opening) {
            ctrl.opening = opening;

            if (ctrl.opening.city) {
                City.get({ id: ctrl.opening.city }, function (city) {
                    ctrl.cityName = city.display_name;
                });
            } else {
                ctrl.cityName = '';
            }

            if (ctrl.opening.students.includes(userId)) {
                ctrl.opening.is_current_user_applied = true;
            }

            if (ctrl.mode === 'student') {
                Vacancy.view({ id: ctrl.openingId });
            }
        });

        VacancyFavorite.query({ vacancy: ctrl.openingId, user: userId }, function (favorites) {
            if (favorites.length > 0) {
                ctrl.favoriteId = favorites[0].id;
            } else {
                ctrl.favoriteId = null;
            }
        }, function () {
            ctrl.favoriteId = null;
        });

        ctrl.$onInit = function () {

        };

        ctrl.applyToJob = function () {
            Vacancy.apply(ctrl.opening, function () {
                if (!ctrl.opening.students.includes(userId)) {
                    ctrl.opening.students.push(userId);
                    ctrl.opening.is_current_user_applied = true;
                }
            });
        };

        ctrl.deleteOpening = function () {
            Vacancy.delete(ctrl.opening, function () {
                $state.go('openings');
            });
        };

        ctrl.getOption = function (options, id) {
            const found = options?.find((option) => option.id == id);
            if (found) {
                return found.name;
            } else {
                return id;
            }
        };

        ctrl.ratingBackground = function (rating) {
            const percent = ctrl.ratingPercent(rating);
            return `linear-gradient(90deg, #F9A224, #F9A224 ${percent}, #D9D9D9 ${percent}, #D9D9D9)`;
        };

        ctrl.ratingPercent = function (rating) {
            return (rating * 20) + '%';
        };

        ctrl.safeHtml = function (html) {
            return $sce.trustAsHtml(html);
        };

        ctrl.toggleFavorite = function () {
            if (ctrl.favoriteId) {
                VacancyFavorite.delete({ id: ctrl.favoriteId }, function () {
                    ctrl.favoriteId = null;
                });
            } else {
                VacancyFavorite.save({ vacancy: ctrl.openingId, user: userId }, function (favorite) {
                    ctrl.favoriteId = favorite.id;
                });
            }
        };
    }
})(angular);
