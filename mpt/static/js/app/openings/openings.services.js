(function (angular) {
    'use strict';

    var module = angular.module('openings.services', ['ngResource']);

    module.factory('Vacancy', function($resource) {
        return $resource(API_URL + '/vacancy/:id', { 'id': '@id' }, {
            'apply': { method: 'POST', url: API_URL + '/vacancy/:id/apply' },
            'update': { method: 'PUT' },
            'view': {  method: 'POST', url: API_URL + '/vacancy/:id/view' },
        });
    });

    module.factory('VacancyCandidates', function($resource) {
        return $resource(API_URL + '/vacancy/:id/candidates', { 'id': '@id' }, {
        });
    });

    module.factory('VacancyFavorite', function($resource) {
        return $resource(API_URL + '/vacancy_favorite/:id', { 'id': '@id' }, {
        });
    });

})(angular);
