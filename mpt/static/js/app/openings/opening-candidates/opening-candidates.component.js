(function(angular) {
    'use strict';

    angular
        .module('openingCandidates')
        .component('openingCandidates', {
            templateUrl: '/opening-candidates.template.html',
            controller: OpeningCandidatesCtrl,
            bindings: {
            },
        });

    OpeningCandidatesCtrl.$inject = [
        '$scope',
        '$sce',
        '$stateParams',
        'CurrentUser',
        'VacancyCandidates',
    ];

    function OpeningCandidatesCtrl (
        $scope,
        $sce,
        $stateParams,
        CurrentUser,
        VacancyCandidates,
    ) {
        const ctrl = this;

        const user = CurrentUser;

        ctrl.openingId = $stateParams.openingId;

        ctrl.students = [];
        ctrl.shownStudents = [];

        VacancyCandidates.query({ id: ctrl.openingId }, function (students) {
            ctrl.students = students;
            ctrl.shownStudents = students.slice(0, 5);
        })

        ctrl.$onInit = function () {

        };

        ctrl.ratingBackground = function (rating) {
            const percent = ctrl.ratingPercent(rating);
            return `linear-gradient(90deg, #F9A224, #F9A224 ${percent}, white ${percent}, white)`;
        };

        ctrl.ratingPercent = function (rating) {
            return (rating * 20) + '%';
        };

        ctrl.safeHtml = function (html) {
            return $sce.trustAsHtml(html);
        };

        ctrl.showAllStudents = function () {
            ctrl.shownStudents = ctrl.students;
        };
    }
})(angular);
