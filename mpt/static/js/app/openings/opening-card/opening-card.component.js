(function(angular) {
    'use strict';

    angular
        .module('openingCard')
        .component('openingCard', {
            templateUrl: '/opening-card.template.html',
            controller: OpeningCardCtrl,
            bindings: {
                knowledgeOptions: '<',
                onTagSelect: '&',
                opening: '<',
                skillOptions: '<',
            },
        });

    OpeningCardCtrl.$inject = [
        '$scope',
        'CurrentUser',
    ];

    function OpeningCardCtrl (
        $scope,
        CurrentUser,
    ) {
        const ctrl = this;

        ctrl.mode = CurrentUser.groups.includes('Mentores') ? 'mentor' : 'student';

        ctrl.isNewOpening = false;
        ctrl.shownTags = [];
        ctrl.tags = [];

        function getOption (options, id) {
            const found = options.find((option) => option.id == id);
            if (found) {
                return found;
            } else {
                return null;
            }
        };

        ctrl.$onInit = function () {
            if (typeof moment !== 'undefined') {
                ctrl.isNewOpening = moment().diff(ctrl.opening.created_at, 'days') < 7;
            }

            for (const knowledge of ctrl.opening.knowledge) {
                const tag = getOption(ctrl.knowledgeOptions, knowledge);
                if (tag) {
                    ctrl.tags.push({ ...tag, tag_type: 'knowledge' });
                }
            }
            for (const skill of ctrl.opening.skills) {
                const tag = getOption(ctrl.skillOptions, skill);
                if (tag) {
                    ctrl.tags.push({ ...tag, tag_type: 'skill' });
                }
            }
            ctrl.shownTags = ctrl.tags.slice(0, 2);
        };

        ctrl.ratingBackground = function (rating) {
            const percent = ctrl.ratingPercent(rating);
            return `linear-gradient(90deg, #F9A224, #F9A224 ${percent}, #D9D9D9 ${percent}, #D9D9D9)`;
        };

        ctrl.ratingPercent = function (rating) {
            return (rating * 20) + '%';
        };

        ctrl.showAllTags = function () {
            ctrl.shownTags = ctrl.tags;
        };
    }
})(angular);
