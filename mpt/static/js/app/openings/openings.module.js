(function (angular) {
    'use strict';

    const module = angular.module('openings', [
        'ui.router',
        'oc.lazyLoad',
    ]);

    module.config(function ($stateProvider) {
        $stateProvider
        .state('openings', {
            url: '/openings',
            component: 'openingsList',
            resolve: {
                lazyLoadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'https://unpkg.com/moment@2.29.1/min/moment-with-locales.min.js',
                        '/static/js/app/openings/openings.services.js',
                        '/static/js/app/openings/openings-list/openings-list.module.js',
                        '/static/js/app/openings/openings-list/openings-list.component.js',
                        '/static/js/app/openings/opening-card/opening-card.module.js',
                        '/static/js/app/openings/opening-card/opening-card.component.js',
                    ], {
                        serie: true,
                    });
                }],
            },
        })
        .state({
            name: 'openings.create',
            url: '/new',
            component: 'openingEdit',
            resolve: {
                lazyLoadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        '/static/js/app/openings/openings.services.js',
                        '/static/js/app/openings/opening-edit/opening-edit.module.js',
                        '/static/js/app/openings/opening-edit/opening-edit.component.js',
                    ], {
                        serie: true,
                    })
                }]
            },
        })
        .state({
            name: 'openings.edit',
            url: '/edit/:openingId',
            component: 'openingEdit',
            resolve: {
                lazyLoadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        '/static/js/app/openings/openings.services.js',
                        '/static/js/app/openings/opening-edit/opening-edit.module.js',
                        '/static/js/app/openings/opening-edit/opening-edit.component.js',
                    ], {
                        serie: true,
                    })
                }]
            },
        })
        .state({
            name: 'openings.opening',
            url: '/:openingId',
            component: 'openingDetail',
            resolve: {
                lazyLoadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        '/static/js/app/openings/openings.services.js',
                        '/static/js/app/openings/opening-detail/opening-detail.module.js',
                        '/static/js/app/openings/opening-detail/opening-detail.component.js',
                    ], {
                        serie: true,
                    })
                }]
            },
        })
        .state({
            name: 'openings.opening.candidates',
            url: '/candidates',
            component: 'openingCandidates',
            resolve: {
                lazyLoadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        '/static/js/app/openings/openings.services.js',
                        '/static/js/app/openings/opening-candidates/opening-candidates.module.js',
                        '/static/js/app/openings/opening-candidates/opening-candidates.component.js',
                    ], {
                        serie: true,
                    })
                }]
            },
        })
    });
})(angular);
