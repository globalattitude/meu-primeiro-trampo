(function(angular) {
    'use strict';

    angular
        .module('openingsList')
        .component('openingsList', {
            templateUrl: '/openings-list.template.html',
            transclude: {
                title: '?openingsListTitle'
            },
            controller: OpeningsListCtrl,
            bindings: {

            },
        });

    OpeningsListCtrl.$inject = [
        '$scope',
        '$sce',
        'CurrentUser',
        'KnowledgeOptions',
        'SkillOptions',
        'Vacancy',
    ];

    function OpeningsListCtrl (
        $scope,
        $sce,
        CurrentUser,
        KnowledgeOptions,
        SkillOptions,
        Vacancy,
    ) {
        const ctrl = this;

        ctrl.user = CurrentUser;

        ctrl.allOpenings = false;
        ctrl.allAppliedOpenings = false;
        ctrl.allFavoriteOpenings = false;

        ctrl.openings = [];
        ctrl.appliedOpenings = [];
        ctrl.favoriteOpenings = [];
        ctrl.filteredAppliedOpenings = [];
        ctrl.filteredFavoriteOpenings = [];
        ctrl.filteredOpenings = [];
        ctrl.shownAppliedOpenings = [];
        ctrl.shownFavoriteOpenings = [];
        ctrl.shownOpenings = [];

        ctrl.filters = {
            tags: [],
            text: '',
        };

        ctrl.mode = ctrl.user.groups.includes('Mentores') ? 'mentor': 'student';

        ctrl.knowledgeOptions = [];
        ctrl.skillOptions = [];
        ctrl.tags = [];

        Promise.all([
            KnowledgeOptions.query({}).$promise,
            SkillOptions.query({}).$promise,
        ]).then(function ([knowledgeOptions, skillOptions]) {
            ctrl.knowledgeOptions = knowledgeOptions;
            ctrl.skillOptions = skillOptions;
            for (const option of knowledgeOptions) {
                ctrl.tags.push({ ...option, tag_type: 'knowledge' });
            }
            for (const option of skillOptions) {
                ctrl.tags.push({ ...option, tag_type: 'skill' });
            }
        });

        function filterOpening (opening) {
            if (ctrl.filters.text) {
                const text = ctrl.filters.text.toLocaleLowerCase();
                if (!matchText(opening.name, text) && !matchText(opening.description, text)) {
                    return false;
                }
            }

            if (ctrl.filters.tags.length > 0) {
                let found = false
                for (const tag of ctrl.filters.tags) {
                    if (tag.tag_type === 'knowledge' && opening.knowledge.includes(tag.id)) {
                        found = true;
                        break;
                    } else if (tag.tag_type === 'skill' && opening.skills.includes(tag.id)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }

            return true;
        }

        function matchText (source, target) {
            return source.toLocaleLowerCase().includes(target);
        }

        function filterOpenings () {
            ctrl.filteredOpenings = ctrl.openings.filter(filterOpening);
            ctrl.filteredAppliedOpenings = ctrl.appliedOpenings.filter(filterOpening);
            ctrl.filteredFavoriteOpenings = ctrl.favoriteOpenings.filter(filterOpening);

            if (!ctrl.allOpenings) {
                ctrl.shownOpenings = ctrl.filteredOpenings.slice(0, 4);
            }
            if (!ctrl.allAppliedOpenings) {
                ctrl.shownAppliedOpenings = ctrl.filteredAppliedOpenings.slice(0, 4);
            }
            if (!ctrl.allFavoriteOpenings) {
                ctrl.shownFavoriteOpenings = ctrl.filteredFavoriteOpenings.slice(0, 4);
            }
        }

        Vacancy.query({}, function (openings) {
            ctrl.openings = openings;
            ctrl.favoriteOpenings = [];
            filterOpenings();
        });

        Vacancy.query({ students:  ctrl.user.id }, function(appliedOpenings) {
            ctrl.appliedOpenings = appliedOpenings;
            filterOpenings();
        });

        ctrl.$onInit = function () {
        };

        ctrl.clearFilters = function () {
            ctrl.filters.tags = [];
            ctrl.filters.text = '';
            filterOpenings();
        };

        ctrl.onTextChange = function () {
            filterOpenings();
        }

        ctrl.safeHtml = function (html) {
            return $sce.trustAsHtml(html);
        };

        ctrl.seeAllOpenings = function () {
            ctrl.allOpenings = true;
            ctrl.allAppliedOpenings = false;
            ctrl.allFavoriteOpenings = false;
            ctrl.shownOpenings = ctrl.openings;
        };

        ctrl.seeAllAppliedOpenings = function () {
            ctrl.allAppliedOpenings = true;
            ctrl.shownAppliedOpenings = ctrl.appliedOpenings;
        };

        ctrl.seeAllFavoriteOpenings = function () {
            ctrl.allFavoriteOpenings = true;
            ctrl.shownFavoriteOpenings = ctrl.favoriteOpenings;
        };

        ctrl.seeLessOpenings = function () {
            ctrl.allOpenings = false;
            ctrl.shownOpenings = ctrl.filteredOpenings.slice(0, 4);
            ctrl.shownAppliedOpenings = ctrl.filteredAppliedOpenings.slice(0, 4);
            ctrl.shownFavoriteOpenings = ctrl.filteredFavoriteOpenings.slice(0, 4);
        };

        ctrl.selectTag = function (tag) {
            if (!ctrl.filters.tags.find((t) => t.id == tag.id)) {
                ctrl.filters.tags.push(tag);
                filterOpenings();
            }
        };

        ctrl.tagExists = function (newTag) {
            for (const tag of ctrl.filters.tags) {
                if (tag.name.toLowerCase() === newTag) {
                    return true;
                }
            }
            return false;
        }
    }
})(angular);
