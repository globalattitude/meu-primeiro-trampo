(function(angular) {
    'use strict';

    var module = angular.module('openingsList', [

    ]);

    module.config( [
        '$compileProvider',
        function($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|webcal):/);
        }
    ]);

})(angular);
