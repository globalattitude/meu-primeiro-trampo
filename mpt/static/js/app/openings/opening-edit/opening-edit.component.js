(function(angular) {
    'use strict';

    angular
        .module('openingEdit')
        .component('openingEdit', {
            templateUrl: '/opening-edit.template.html',
            controller: OpeningEditCtrl,
            bindings: {
                opening: '<',
            },
        });

    OpeningEditCtrl.$inject = [
        '$scope',
        '$state',
        '$stateParams',
        'City',
        'CityAutocomplete',
        'CurrentUser',
        'FormUpload',
        'KnowledgeOptions',
        'SkillOptions',
        'Vacancy',
        'gettextCatalog',
    ];

    function OpeningEditCtrl (
        $scope,
        $state,
        $stateParams,
        City,
        CityAutocomplete,
        CurrentUser,
        FormUpload,
        KnowledgeOptions,
        SkillOptions,
        Vacancy,
        gettextCatalog,
    ) {
        const ctrl = this;

        const user = CurrentUser;

        if ($state.is('openings.create')) {
            ctrl.mode = 'create';
        } else {
            ctrl.mode = 'edit';
            ctrl.openingId = $stateParams.openingId;
        }

        ctrl.cityName = '';
        ctrl.opening = null;

        ctrl.tinymceOptions = {
            plugins: 'link',
            toolbar: 'undo redo | bold italic | link removeformat',
        };

        KnowledgeOptions.query({}, function (knowledgeOptions) {
            ctrl.knowledgeOptions = knowledgeOptions;
        });

        SkillOptions.query({}, function (skillOptions) {
            ctrl.skillOptions = skillOptions;
        });

        function getOptions (dict) {
            return Object.entries(dict).map((entry) => {
                return { label: gettextCatalog.getString(entry[1]), slug: entry[0] };
            });
        }

        ctrl.options = {
            benefits: [
                'Assistência médica',
                'Assistência odontológica',
                'Vale-refeição',
                'Vale-transporte',
            ],
            contract_type: getOptions({
                'effective': 'Effective',
                'internship': 'Internship',
                'apprentice': 'Apprentice',
            }),
            presence: getOptions({
                'presential': 'Presential',
                'hybrid': 'Hybrid',
                'remote': 'Remote',
            }),
            workday: getOptions({
                'full_time': 'Full time',
                'part_time_morning': 'Party time - Morning',
                'part_time_afternoon': 'Party time - Afternoon',
                'part_time_night': 'Party time - Night',
                'night_time': 'Night time',
            }),
        };

        ctrl.$onInit = function () {
            if (ctrl.openingId) {
                Vacancy.get({ id: ctrl.openingId }, function (opening) {
                    ctrl.opening = opening;
                    ctrl.opening.id = ctrl.openingId;

                    if (ctrl.opening.city) {
                        City.get({ id: ctrl.opening.city }, function (city) {
                            ctrl.cityName = city.display_name;
                        });
                    } else {
                        ctrl.cityName = '';
                    }
                }, function () {
                    ctrl.opening = newOpening();
                });
            } else {
                ctrl.opening = newOpening();
            }
        };

        function newOpening() {
            return {
                author: user.id,
                benefits: [],
                city: null,
                company: {
                    contact_email: '',
                    contact_link: '',
                    contact_name: '',
                    email: '',
                    // logo: '',
                    name: '',
                    phone_number: '',
                },
                contract_type: null,
                description: '',
                knowledge: [],
                name: '',
                presence: null,
                salary: '',
                salary_to_be_defined: false,
                skills: [],
                workday: null,
            };
        }

        ctrl.getCities = function (val) {
            return new CityAutocomplete(val);
        };

        ctrl.onSelectCity = function (val) {
            ctrl.opening.city = val.id;
        };

        ctrl.saveOpening = function () {
            const action = (ctrl.mode === 'edit') ? 'update' : 'save';

            const companyLogo = ctrl.opening.company.logo;
            delete ctrl.opening.company.logo;

            Vacancy[action](ctrl.opening, function (opening) {
                ctrl.errors = {};

                ctrl.uploadLogo(opening, companyLogo).then(() => {
                    $state.go('openings.opening', { openingId: opening.id });
                })

            }, function (errors) {
                console.error(errors);
                ctrl.opening.company.logo = companyLogo;
                ctrl.errors = errors.data;
            });
        };

        ctrl.uploadLogo = function (opening, logo) {
            if (!logo) {
                return Promise.resolve();
            }

            const fu = new FormUpload();
            fu.addField('image', logo);

            return fu.sendTo(API_URL + '/vacancy/' + opening.id + '/image');
        };
    }
})(angular);
