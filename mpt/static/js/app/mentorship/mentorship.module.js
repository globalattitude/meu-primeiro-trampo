(function (angular) {
    'use strict';

    const module = angular.module('mentorship', [
        'ui.router',
        'oc.lazyLoad',
    ]);

    module.config(function ($stateProvider) {
        $stateProvider
        .state('mentorships', {
            url: '/mentorship',
            component: 'mentorshipsList',
            resolve: {
                lazyLoadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        '/static/js/app/mentorship/mentorship.services.js',
                        '/static/js/app/mentorship/mentorships-list/mentorships-list.module.js',
                        '/static/js/app/mentorship/mentorships-list/mentorships-list.component.js',
                    ], {
                        serie: true,
                    });
                }],
            },
        })
    });
})(angular);
