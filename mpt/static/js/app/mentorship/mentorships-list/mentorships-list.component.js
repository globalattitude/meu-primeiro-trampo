(function(angular) {
    'use strict';

    angular
        .module('mentorshipsList')
        .component('mentorshipsList', {
            templateUrl: '/mentorships-list.template.html',
            transclude: {
                title: '?mentorshipsListTitle'
            },
            controller: MentorshipsListCtrl,
            bindings: {

            },
        });

    MentorshipsListCtrl.$inject = [
        '$scope',
        '$sce',
        'CurrentUser',
        'MentorStudent',
        'MptProfile',
        'TranslationService',
    ];

    function MentorshipsListCtrl (
        $scope,
        $sce,
        CurrentUser,
        MentorStudent,
        MptProfile,
        TranslationService
    ) {
        const ctrl = this;

        const user = CurrentUser;

        let locale = TranslationService.currentLanguage;
        $scope.$on('languageChanged', function(event, data) {
            locale = data.newLanguage;
        });

        MentorStudent.query({ mentor: user.id }, function (mentored) {
            ctrl.mentored = mentored;
        });

        ctrl.$onInit = function () {

        };

        ctrl.safeHtml = function (html) {
            return $sce.trustAsHtml(html);
        };
    }
})(angular);
