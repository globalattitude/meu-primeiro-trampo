(function(angular) {
    'use strict';

    angular.module('course-intro', [
        'shared',
        'course-intro.controller',
        'eventsList',
        'ngFileUpload',
        'ui.tinymce',
    ]);
})(angular);
