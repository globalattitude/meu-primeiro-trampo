(function (angular) {
    'use strict';

    const app = angular.module('course-intro.controller', []);

    app.controller('CourseIntroController', [
        '$scope',
        '$window',
        'CurrentUser',
        function ($scope, $window, CurrentUser) {
            $scope.user = CurrentUser;

            $scope.$on('languageChanged', (event, data) => {
                if (data.newLanguage !== data.oldLanguage) {
                    $window.location.reload();
                }
            });
        }
    ]);
})(angular);
