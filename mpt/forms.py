from allauth.account.forms import SignupForm
from django import forms
from baquara.users.models import User, GenderOption, SexualOrientationOption, RaceOption
from django.utils.translation import gettext_lazy as _
from cities_light.models import City

class DateInput(forms.DateInput):
    input_type = 'date'

class CustomSignupForm(SignupForm):
    preferred_language = forms.ChoiceField(choices=User.LANGUAGES_CHOICES)
    accepted_terms = forms.BooleanField(initial=False, required=False)
    birth_date = forms.DateField( required=True, widget=DateInput(
        attrs={
            'title' : _('Birth Date'),
            'class':'form-control',
        }
    ))
    name = forms.CharField(max_length=128)
    gender = forms.ModelChoiceField(queryset = GenderOption.objects.all(),empty_label=_("Select your gender"), widget=forms.Select(
        attrs={
            'class':'form-control',
        }
    ) )
    city = forms.ModelChoiceField(queryset = City.objects.all(),empty_label=_("Inform your town of residence"), widget=forms.Select(
        attrs={
            'class':'form-control',
        }
    ) )
    race = forms.ModelChoiceField(queryset = RaceOption.objects.all(), empty_label=_("Tell us how you identify yourself"), widget=forms.Select(
        attrs={
            'class':'form-control',
        }
    ) )
    institution = forms.CharField(max_length=128)
    phone_number = forms.CharField(max_length=128)
    cpf =  forms.CharField(max_length=128)


    def save(self, request):
        user = super(CustomSignupForm, self).save(request)
        user.gender = self.cleaned_data['gender']
        user.race = self.cleaned_data['race']
        user.birth_date = self.cleaned_data['birth_date']
        user.city = self.cleaned_data['city']
        user.preferred_language = self.cleaned_data['preferred_language']
        user.accepted_terms = self.cleaned_data['accepted_terms']
        user.name = self.cleaned_data['name']
        user.institution = self.cleaned_data['institution']
        user.phone_number = self.cleaned_data['phone_number']
        user.cpf = self.cleaned_data['cpf']
        user.save()

        return user
