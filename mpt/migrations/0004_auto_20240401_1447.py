# Generated by Django 2.2.28 on 2024-04-01 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mpt', '0003_auto_20240401_1308'),
    ]

    operations = [
        migrations.AddField(
            model_name='vacancy',
            name='salary',
            field=models.CharField(blank=True, max_length=255, verbose_name='Salary'),
        ),
        migrations.AddField(
            model_name='vacancy',
            name='salary_to_be_defined',
            field=models.BooleanField(default=False, verbose_name='Salary to be defined'),
        ),
    ]
