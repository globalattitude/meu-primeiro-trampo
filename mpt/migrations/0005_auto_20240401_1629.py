# Generated by Django 2.2.28 on 2024-04-01 19:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mpt', '0004_auto_20240401_1447'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profilecourse',
            old_name='couse_name',
            new_name='course_name',
        ),
    ]
