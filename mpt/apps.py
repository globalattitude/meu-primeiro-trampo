from django.apps import AppConfig


class mptConfig(AppConfig):
    name = 'mpt'
    verbose_name = 'mpt'
