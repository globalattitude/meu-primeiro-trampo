=============================
Baquara
=============================

Development (with Docker)
--------------------------

Now, you're ready to get your stack up and running
::
    docker-compose up


Aditional notes
~~~~~~~~~~~~~~~~~
If you would like to have interactive control for debuging in your server log (pdb or ipdb support), you must activate the stack with the following command
::
    docker-compose run --service-ports --rm web


Deploy a production database on your local environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Put the dump file given to you by the Hacklab/ team on the installation directory.

Copy the file to the database container (do it while the application runs).
::
    docker cp dump.psqlc <directory_name>_db_1:/tmp

Now, enter in the container and run the restore command.
::
    docker exec -it <directory_name>_db_1 bash
    # you're inside the container now
    su postgres
    pg_restore -O -c -x -n public -d timtec /tmp/dump.psqlc
