#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

# Uncomment the following line if you want to user baquara code for dev porpouses
pip install -r /app/requirements/local.txt

pip install -e /app/mpt

python manage.py migrate
python manage.py runserver 0.0.0.0:8000
